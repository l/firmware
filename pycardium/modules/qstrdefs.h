#include "py/mpconfig.h"

#ifndef Q
#define Q(x)
#endif

/* leds */
Q(leds)
Q(BOTTOM_LEFT)
Q(BOTTOM_RIGHT)
Q(TOP_LEFT)
Q(TOP_RIGHT)

/* utime */
Q(utime)
Q(sleep)
Q(sleep_ms)
Q(sleep_us)
Q(ticks_ms)
Q(ticks_us)
Q(ticks_cpu)
Q(ticks_add)
Q(ticks_diff)

/* vibra */
Q(vibra)