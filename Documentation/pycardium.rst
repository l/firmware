Pycardium
=========

Pycardium is a core1 payload based on MicroPython.  It can interface with
card10 using the Epicardium API, which is wrapped in a bunch of python modules.
These modules are documented in this section.

.. toctree::
   :maxdepth: 1
   :caption: Modules:

   pycardium/color
   pycardium/leds
