card10 firmware docs
====================

This is the API documentation for card10's default firmware.

Overview
--------

The design roughly looks like this:

.. image:: static/overview.svg

.. toctree::
   :maxdepth: 2
   :caption: Contents

   epicardium
   pycardium
