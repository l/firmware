Epicardium API
==============

.. ifconfig:: not has_hawkmoth

   .. warning::

      This version of the documentation was built without
      `Hawkmoth <https://github.com/jnikula/hawkmoth>`_ available.  This means,
      no documentation for the Epicardium API was generated.

.. c:autodoc:: epicardium/epicardium.h
